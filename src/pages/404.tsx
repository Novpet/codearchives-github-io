import * as React from 'react'

const Error404 = () => (
    <h1>Page not found!</h1>
)

export default Error404